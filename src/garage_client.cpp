#include <ros/ros.h>
#include <actionlib_example/TriggerAction.h>
#include <actionlib/client/simple_action_client.h>

void donecb(const actionlib::SimpleClientGoalState& state, 
    const actionlib_example::TriggerResultConstPtr& result){
        ROS_INFO("got result: %s", result->open ? "true" : "false");
}

void feedbackcb(const actionlib_example::TriggerFeedbackConstPtr& fb){
    ROS_INFO_STREAM("got feedback:" << fb->position);
}

void activecb(){
    ROS_INFO("active");
}

int main(int argc, char** argv){
    ros::init(argc, argv, "gar_cli");
    ros::NodeHandle nh;
    ROS_INFO("client here");
    actionlib::SimpleActionClient<actionlib_example::TriggerAction> ac("garage_server");
    ac.waitForServer();
    actionlib_example::TriggerGoal goal;
    goal.open = true;
    ac.sendGoal(goal, donecb, activecb, feedbackcb);
    ROS_INFO("sent goal %s", goal.open ? "true" : "false");
    ros::Duration(4).sleep();
    goal.open = false;
    ac.sendGoal(goal, donecb, activecb, feedbackcb);
    ROS_INFO("sent goal %s", goal.open ? "true" : "false");
    ac.waitForResult();
    //ros::spin();
    return 0;
}
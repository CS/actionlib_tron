#include <ros/ros.h>
#include <actionlib_example/TriggerAction.h>
#include "tron_adapter.h"
#include <std_msgs/Int32.h>

TRON_Adapter adapter;

void send_goal (Mapping& map, int32_t* val, bool open){
    auto shared_ptr = boost::make_shared<actionlib_example::TriggerActionGoal>();
    shared_ptr->goal.open = open;
    adapter.publish_to_topic<actionlib_example::TriggerActionGoal>(map.topic, shared_ptr);
}
void send_goal_open(Mapping& map, int32_t* val) {send_goal(map, val, true);}
void send_goal_close(Mapping& map, int32_t* val) {send_goal(map, val, false);}
void feedback_callback(const boost::shared_ptr<actionlib_example::TriggerActionFeedback> ptr){
    for (Mapping& map : adapter.mappings) {
        if (map.channel.name == "position" && map.channel.is_input == false)
            adapter.report_now(map.channel, 1, &ptr->feedback.position);
    }
}
void result_callback(const boost::shared_ptr<actionlib_example::TriggerActionResult> ptr){
    for (Mapping& map : adapter.mappings) {
        if (map.channel.name == "fertig" && map.channel.is_input == false)
            adapter.report_now(map.channel, 1, &ptr->result.position);
    }
}

void configuration_phase(ros::NodeHandle& nh){
    /* note: for configuration phase maximum message length is 256 Bytes, 
    therefore heap allocation can be avoided most of the time in called functions */
    
    // note: since we are not an actual client (or server) 
    // we need to use the high level packet::*Action* messages.
    // custom callbacks are implemented in order to not worry about header size
    Mapping map = adapter.createMapping("/garage_server/goal", "oeffnen", true);
    map.input_callback = send_goal_open;
    adapter.mappings.push_back(map);

    map = adapter.createMapping("/garage_server/goal", "schliessen", true);
    map.input_callback = send_goal_close;
    adapter.mappings.push_back(map);

    adapter.input_publishers.push_back(nh.advertise<actionlib_example::TriggerActionGoal>("/garage_server/goal", 1));

    map = adapter.createMapping("/garage_server/result", "fertig", false);
    adapter.add_var_to_mapping(map, "akt_position");
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/garage_server/result", 100, result_callback));

    map = adapter.createMapping("/garage_server/feedback", "position", false);
    adapter.add_var_to_mapping(map, "akt_position");
    adapter.mappings.push_back(map);
    adapter.output_subscribers.push_back(nh.subscribe("/garage_server/feedback", 100, 
            feedback_callback));
    // not obvious in documentation: local variables are not supported
    uint64_t microseconds = 1000000; // one second
    // documentation states 2 signed integers are used for some reason
    adapter.set_time_unit_and_timeout(microseconds, 300);

    // wait till subscribers initialized
    for (ros::Publisher& pub : adapter.input_publishers) {
        while (pub.getNumSubscribers() == 0) { ros::Duration(1).sleep(); };
    }
}

int main(int argc, char**argv){
    ros::init(argc, argv, "TRON dapter");
    ros::NodeHandle nh;

    try {
        const std::string IP = "127.0.0.1";
        const uint16_t PORT = 8080;
        adapter = TRON_Adapter(IP, PORT);
        
        configuration_phase(nh);
        
        // example use of template callbacks
        /*
        Mapping map = adapter.createMapping("/test_in", "oeffnen", true);
        adapter.add_var_to_mapping(map, "akt_position");
        map.input_callback = boost::bind(&TRON_Adapter::mapping_callback_to_topic<std_msgs::Int32>, &adapter, _1, _2);
        adapter.input_publishers.push_back(nh.advertise<std_msgs::Int32>("/test_in", 10));
        adapter.mappings.push_back(map);

        map = adapter.createMapping("/test_out", "position", false);
        adapter.add_var_to_mapping(map, "akt_position");
        adapter.output_subscribers.push_back(nh.subscribe<std_msgs::Int32>("/test_out", 10, 
            boost::function<void(const ros::MessageEvent<std_msgs::Int32>&)>(
            boost::bind(&TRON_Adapter::mappings_callback_to_TRON<std_msgs::Int32>, &adapter, _1))));
        adapter.mappings.push_back(map);
        adapter.set_time_unit_and_timeout(1000000L, 300);
        */

        adapter.request_start(); 
        
        // testing phase loop
        ros::Rate test_phase_freq(60);
        while (ros::ok()) {
            ros::spinOnce();
            adapter.process_TRONs_msgs();
            test_phase_freq.sleep();
        } 
    } catch (const char* err){
        ROS_FATAL("shutting down: %s", err);
        ros::shutdown();
    } catch (...){
    	ROS_FATAL("shutting down");
    	ros::shutdown();
    }
}

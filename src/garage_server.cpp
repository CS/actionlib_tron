#include <ros/ros.h>
#include <actionlib_example/TriggerAction.h>
#include <actionlib/server/simple_action_server.h>
#include <stdlib.h>
#include <std_msgs/Int32.h>

class Garage {
    private:
        int32_t current_position;
        actionlib::SimpleActionServer<actionlib_example::TriggerAction> as;
        actionlib_example::TriggerFeedback fb;
        actionlib_example::TriggerResult res;
        void (*move) (int32_t*);
        static void move_up(int32_t* ptr) {(*ptr)++;};
        static void move_down(int32_t* ptr) {(*ptr)--;};
    public:
        static const int OPEN = 1000;
        static const int CLOSED = 0;

        Garage() : as("garage_server", boost::bind(&Garage::execute, this, _1), false){
            current_position = 0;
            move = Garage::move_up;
            this->as.start();
        }

        ~Garage() {}

        void execute(const actionlib_example::TriggerGoalConstPtr& goal){
            int to_reach;
            if (goal->open == true) {
                to_reach = OPEN;
                move = move_up;
            }
            else {
                to_reach = CLOSED;
                move = move_down;
            }
            ros::Rate rate(100);
            while (true) {
                rate.sleep();
                if (as.isPreemptRequested() || !ros::ok() || as.isNewGoalAvailable()) {
                    ROS_INFO("server preempted at position %i", current_position);
                    res.position = current_position;
                    as.setPreempted(res, "preempted");
                    return;
                }
                if (current_position == to_reach) {
                    res.open = to_reach == OPEN;
                    res.position = current_position;
                    ROS_INFO("server goal reached");
                    as.setSucceeded(res);
                    return;
                }
                move(&this->current_position);
                // some random feedback, 1% chance
                if (rand()/((float) RAND_MAX) < 0.01) {
                    fb.position = current_position;
                    ROS_INFO("sending feedback: %i", current_position);
                    as.publishFeedback(fb);
                }
            }         
        }
};


/*
ros::Publisher pub;
void test_in_cb(ros::MessageEvent<std_msgs::Int32> event) {
    std_msgs::Int32 i;
    i.data = 100;
    pub.publish(i);
}
*/

int main(int argc, char** argv){
    ros::init(argc, argv, "gar_serv");
    
    ros::NodeHandle nh;
    ROS_INFO("server here");
    // not working on stack for some reason, 
    // might be too big, no stackoverflow thrown though
    // Garage gar();
    Garage* gar = new Garage();
    /*
    pub = nh.advertise<std_msgs::Int32>("test_out", 10);
    ros::Subscriber sub = nh.subscribe<std_msgs::Int32>("test_in", 10, test_in_cb);
    */
    ros::spin();
    delete gar;
    return 0;
}